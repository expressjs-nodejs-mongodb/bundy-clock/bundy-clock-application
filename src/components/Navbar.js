import { Component } from "react";
import "./NavbarStyles.css";

import {NavLink} from 'react-router-dom';
import {Nav} from 'react-bootstrap';

import ICON from '../Images/ALz-icon.png';

import {useContext} from 'react';
import UserContext from '../UserContext';


export default function Navbar(){
  const {user} = useContext(UserContext);

  return(
    <>
         <nav>
          <a href="/">
          <img src={ICON} alt="ALZ" className="icon-alz"></img>
          </a>
         <div>
           <ul id="navbar-items">
           <Nav.Link as={NavLink} exact to="/" className='mr-auto'></Nav.Link>
           {
				  (user.token  === null)?
          <>
          <li>
            <Nav.Link as={NavLink} exact to="/login">LogIn</Nav.Link>
          </li>
          <li>
            <Nav.Link as={NavLink} exact to="/register">Register</Nav.Link>
          </li>
          </>

          :
          (user.token  === null  && user.isAdmin === false)?
          <>
          <li>
            <Nav.Link as={NavLink} exact to="/timelogs">Time Logs</Nav.Link>
          </li>
          <li>
            <Nav.Link as={NavLink} exact to="/logout">LogOut</Nav.Link>
          </li>
          </>
          :
          <>
          <li>
          <Nav.Link as={NavLink} exact to="/admin">Admin</Nav.Link>
          </li>
          <li>
            <Nav.Link as={NavLink} exact to="/admintl">Admin TL</Nav.Link>
          </li>
          <li>
            <Nav.Link as={NavLink} exact to="/timelogs">Time Logs</Nav.Link>
          </li>
          <li>
            <Nav.Link as={NavLink} exact to="/logout">LogOut</Nav.Link>
          </li>
          </>
          }
          
           
            </ul>
          </div>
        </nav>
    </>
  )
}