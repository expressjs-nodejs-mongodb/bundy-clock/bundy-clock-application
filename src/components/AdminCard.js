import {Card} from 'react-bootstrap'
import {Link} from 'react-router-dom'

import {useContext} from 'react';
import UserContext from '../UserContext';

import './AdminCardStyles.css';

export default function Admincard({timelogs}){
	
	const {user} = useContext(UserContext);
	// Checks to see if the data was successfully passed
	console.log(timelogs);
	console.log(typeof timelogs);

	// Destructuring the data to avoid dot notation
	const {fName, lName, email, cntct_num, date_reg, _id} = timelogs;

    let first = timelogs.fName;
    let last = timelogs.lName;
    let fullN = first + " " + last;

	return(
		<>
		
		<div className='admin-card-main'>
		<div className='admin-card-layer2'>
		<Card className=" admin-card " > 
         {/* <Card.Img variant="top" src={imageURL} /> */}
	        <Card.Body className="body-card" >
            <Card.Subtitle>Full Name:</Card.Subtitle>
	            <Card.Text>{fullN}</Card.Text>

                {/* <Card.Subtitle>First:</Card.Subtitle>
	            <Card.Text>{fName}</Card.Text>
                <Card.Subtitle>Last:</Card.Subtitle>
	            <Card.Text>{lName}</Card.Text> */}

				<Card.Subtitle>Email:</Card.Subtitle>
	            <Card.Text>{email}</Card.Text>
                <Card.Subtitle>Number:</Card.Subtitle>
	            <Card.Text>{cntct_num}</Card.Text>
                <Card.Subtitle>Registration Date:</Card.Subtitle>
	            <Card.Text>{date_reg}</Card.Text>
	        </Card.Body>  
			<div>
			<Link className="btn btn-primary" to={`/admintl/${email}`}>View Time Logs Details</Link>	
			</div>      
	    </Card>
		</div>
		</div>
        </>
		)
}