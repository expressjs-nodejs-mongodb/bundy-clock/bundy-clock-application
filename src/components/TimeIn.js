import {Card, Col } from 'react-bootstrap'
// import {Link} from 'react-router-dom'
// import {NavLink} from 'react-router-dom';

import {useContext} from 'react';
import UserContext from '../UserContext';

import './Time-I-O-cards.css';

export default function TimeIn({timelogs}){
	
	const {user} = useContext(UserContext);
	// Checks to see if the data was successfully passed
	console.log(timelogs);
	console.log(typeof timelogs);

	// Destructuring the data to avoid dot notation
	const {email, time_in, date_log, time_logs_confirm, _id} = timelogs;


	return(
		<>
		<div className='productcardcolCss'>
		<div className='second-div'>
		<Card className=" tcardCss" > 
         {/* <Card.Img variant="top" src={imageURL} /> */}
	        <Card.Body className="body-card" >
				<Card.Subtitle>Email:</Card.Subtitle>
	            <Card.Text>{email}</Card.Text>
            	<Card.Subtitle>Time In:</Card.Subtitle>
	            <Card.Text>{time_in}</Card.Text>
	            <Card.Subtitle>Time Log:</Card.Subtitle>
	            <Card.Text>{date_log}</Card.Text>
                <Card.Subtitle>Confirmation:</Card.Subtitle>
	            <Card.Text>{time_logs_confirm}</Card.Text>
	        </Card.Body>        
	    </Card>
		</div>
		</div>
        </>
		)
}