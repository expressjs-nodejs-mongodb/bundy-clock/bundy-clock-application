import TimeIn from '../components/TimeIn.js'
import TimeOuts from '../components/TimeOut.js'

import { useEffect, useState, useContext } from 'react'

import { Row, Button, Form, Col} from 'react-bootstrap';

import Swal from "sweetalert2";

import './TimeLogsStyles.css';

import UserContext from '../UserContext';

import { useNavigate} from "react-router-dom";

export default function TimeLogs() {

    const {user} = useContext(UserContext);

    const navigate = useNavigate();
   
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SHOW ALL TIME-IN
	const [timeio, settimeio] = useState([]);
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/timeio/retrieve`,{
        method: "GET",
        headers:{
            "Content-Type": "application/json",
            Authorization: `Bearer ${ localStorage.getItem('token') }`
        },
        })
	        .then(res => res.json())
	        .then(data => {
	            
	            console.log(data);

	            settimeio(data.map(timeio => {
	            	return(
	            		<TimeIn key={timeio._id} timelogs={timeio}/>
	            		)
	            }))

	      });
	}, [])



///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SHOW ALL TIME-OUT
	const [timeouts, settimeouts] = useState([]);
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/timeout/retrieve`,{
        method: "GET",
        headers:{
            "Content-Type": "application/json",
            Authorization: `Bearer ${ localStorage.getItem('token') }`
        },
        })
	        .then(res => res.json())
	        .then(data => {
	            
	            console.log(data);

	            settimeouts(data.map(timeouts => {
	            	return(
	            		<TimeOuts key={timeouts._id} timelogs={timeouts}/>
	            		)
	            }))

	      });
	}, [])
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TIME IN
    function timein(e){
        e.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/timeio/time_in`,{
        method: "POST",
        headers:{
            "Content-Type": "application/json",
            Authorization: `Bearer ${ localStorage.getItem('token') }`
        },
        body: JSON.stringify({       
        })
    })
    .then(res => res.json())
    .then(data => {
        console.log(data);

        if(data){
            Swal.fire({
                title: "Time In Successful",
                icon: "success",
                text: "Have a Nice Day!!"
            });
        }
        else{
            Swal.fire({
                title: "Something went wrong",
                icon: "error",
                text: "Please try again."
            });

        }
    })
    }
//////////////////////////////////////////////////////////////////////////////////////////////////////
// TIME OUT
    function timeoutend(e){
        e.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/timeout/time_out`,{
        method: "POST",
        headers:{
            "Content-Type": "application/json",
            Authorization: `Bearer ${ localStorage.getItem('token') }`
        },
        body: JSON.stringify({       
        })
    })
    .then(res => res.json())
    .then(data => {
        console.log(data);

        if(data){
            Swal.fire({
                title: "Time oUT Successful",
                icon: "success",
                text: "Have a Nice Day!!"
            }); 
        }
        else{
            Swal.fire({
                title: "Something went wrong",
                icon: "error",
                text: "Please try again."
            });

        }
    })
   


    }





	
    return (
        <>
        <div className='home-css'>
        
        <div className='in-out-list'>
        <Row>
            <Col>        <div className='in'>
            <Form onSubmit={e => timein(e)}>
            <Button id='loginbtnCss submitBtn' className="my-3" variant="success" type="submit">
                    Time in
            </Button>
            </Form>
        </div></Col>
            <Col>        <div className='out '>
            <Form onSubmit={e => timeoutend(e)}>
            <Button id='loginbtnCss submitBtn' className="my-3" variant="success" type="submit">
                    Time Out
            </Button></Form>
        </div></Col>
        </Row>
        <div>
        <Row>
        <Col className='roll-left'>
        <div className='time-in-div'>
        <div className='time-in-div1'>{timeio}</div>
         
        </div>
        </Col>


        <Col className='roll-left'>
        <div className='time-in-div'>
        <div className='time-out-div1'>{timeouts}</div> 
        </div>
        </Col>
        </Row>
        </div>
        </div>

        </div>
        </>	
    )
}
