import {useContext, useEffect} from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout(){

	//Consume the UserContext object and destructure it to access the user state and unsetUser function from the context provider
	const {unsetUser, setUser} = useContext(UserContext);

	//Clear the localStorage of the user's information
	unsetUser();

	// By adding the useEffect, this will allow the Logout page to render first before triggering the useEffect which changes the state of our user 
	useEffect(() => {

		//Set the user state back to its original value
		// setUser({id: null})
		setUser({token:null,id:null,isAdmin: null})
	})


	return(
			 // Redirect back to login
			<Navigate to="/login"/>
		)
}