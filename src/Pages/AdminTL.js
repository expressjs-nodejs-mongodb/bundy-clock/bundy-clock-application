import TimeIn from '../components/TimeIn.js'
import TimeOuts from '../components/TimeOut.js'

import { useEffect, useState, useContext } from 'react'

import { Row, Button, Form, Col} from 'react-bootstrap';

import Swal from "sweetalert2";

import './TimeLogsStyles.css';

import UserContext from '../UserContext';

import { useNavigate} from "react-router-dom";

import { useParams} from 'react-router-dom'

export default function TimeLogs() {

    const {user} = useContext(UserContext);

    const navigate = useNavigate();
   
    const { email } = useParams();
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SHOW ALL TIME-IN
	const [timeio, settimeio] = useState([]);
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/timeio/${email}`,{
        method: "GET",
        headers:{
            "Content-Type": "application/json",
            Authorization: `Bearer ${ localStorage.getItem('token') }`
        },
        })
	        .then(res => res.json())
	        .then(data => {
	            
	            console.log(data);

	            settimeio(data.map(timeio => {
	            	return(
	            		<TimeIn key={timeio.email} timelogs={timeio}/>
	            		)
	            }))

	      });
	}, [])



///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SHOW ALL TIME-OUT
	const [timeouts, settimeouts] = useState([]);
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/timeout/${email}`,{
        method: "GET",
        headers:{
            "Content-Type": "application/json",
            Authorization: `Bearer ${ localStorage.getItem('token') }`
        },
        })
	        .then(res => res.json())
	        .then(data => {
	            
	            console.log(data);

	            settimeouts(data.map(timeouts => {
	            	return(
	            		<TimeOuts key={timeouts.email} timelogs={timeouts}/>
	            		)
	            }))

	      });
	}, [])
/////////////////////////////////////////////////////////////////////////////////////////////////////////////






	
    return (
        <>
        <div className='home-css'>
        
        <div className='in-out-list'>
        <div>
        <Row>
        <Col className='roll-left'>
        <div className='time-in-div'>
        <div className='time-in-div1'>{timeio}</div>
         
        </div>
        </Col>


        <Col className='roll-left'>
        <div className='time-in-div'>
        <div className='time-out-div1'>{timeouts}</div> 
        </div>
        </Col>
        </Row>
        </div>
        </div>

        </div>
        </>	
    )
}
