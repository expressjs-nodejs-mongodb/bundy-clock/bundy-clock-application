import './AdminStyles.css';

import { useEffect, useState, useContext } from 'react';

import Admincard from '../components/AdminCard.js';
import { Col, Row } from 'react-bootstrap';

export default function Admin(){

    // SHOW ALL USER
	const [timeio, settimeio] = useState([]);
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/retrieveall`,{
        method: "GET",
        headers:{
            "Content-Type": "application/json",
            Authorization: `Bearer ${ localStorage.getItem('token') }`
        },
        })
	        .then(res => res.json())
	        .then(data => {
	            
	            console.log(data);

	            settimeio(data.map(timeio => {
	            	return(
	            		<Admincard key={timeio._id} timelogs={timeio}/>
	            		)
	            }))

	      });
	}, [])



///////////////////////////////////////////////////////////////////////////////////////////////////////////////

    return(
        <>
        <Row className='admin-main-display'>
            {/* <Col className='col-lg-4'> */}
            <div className='admin-display'>
                {timeio}
            </div>
            {/* </Col> */}
        </Row>
        </>
    )
};