import { Form, Button} from 'react-bootstrap';

import './LogInStyles.css';

import { useState, useEffect, useContext } from 'react';
import { useNavigate} from "react-router-dom";
import UserContext from '../UserContext';
import Swal from 'sweetalert2'


export default function LogIn() {

	const navigate = useNavigate();

	//Allow us to consume the User Context object and its properties to use for user validation
	const {user, setUser} = useContext(UserContext);

	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
	console.log(email);
    console.log(password);

    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);


	const retrieveToken = localStorage.getItem("token");
	const [isAdmin, setIsAdmin] = useState("");


  

 

    useEffect(() => {

	    // Validation to enable submit button when all fields are populated and both passwords match
	    if(email !== '' && password !== ''){
	        setIsActive(true);
	    }else{
	        setIsActive(false);
	    }
	}, [email, password]);


	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/retrieve`, {
		   headers: {
			  Authorization: `Bearer ${token}`
		   },
		})
		   .then((res) => res.json())
		   .then((data) => {
			  console.log(data);
  
			  setUser({
				 id: data._id,
				 isAdmin: data.isAdmin

			  });
		   });
	 };
  

	function authenticate(e) {
	    e.preventDefault();

	    fetch('http://localhost:4000/users/logIn', {
	    	method: 'POST',
	    	headers: {
	    		'Content-Type': 'application/json'
	    	},
	    	body: JSON.stringify({
	    		email: email,
	    		password: password
	    	})
	    })
    	.then(result => result.json())
    	.then(data => {

    		// It is good practice to always print out the result of our fetch request to ensure that the correct information is received in our frontend application
    		console.log(data)

			if(typeof data.access !== "undefined"){
				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access);
				Swal.fire({
					title: "Authentication Success",
					icon: "success",
				
				})
					navigate("/timelogs");	
			}else{
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your login details and try again!"
				})
			}

    	})

	    // Clear input fields after submission
	    setEmail(email);
	    setPassword('*****');
	}

    return (
		<>
		<div className='loginConCss'>
	        {/* <h1 id='h1login'>LOGIN</h1> */}
			<Form className='loginCss' onSubmit={(e) => authenticate(e)}>
				<div className='email-css'>
		        <Form.Group controlId="userEmail">
		            <Form.Label id='labelloginCss'>Email address</Form.Label>
		            <Form.Control 
		                type="email" 
		                placeholder="Enter email"
		                value={email}
		    			onChange={(e) => setEmail(e.target.value)}
		                required/>
		        </Form.Group>
				</div>
				<div className='pass-css'>
		        <Form.Group controlId="password">
		            <Form.Label  id='labelloginCss'>Password</Form.Label>
		            <Form.Control 
		                type="password" 
		                placeholder="Password"
						value={password}
		    			onChange={(e) => setPassword(e.target.value)}
		                required/>
		        </Form.Group>
				</div>

				
	            { 
	            	(isActive) ? 
		                <Button id='loginbtnCss submitBtn' className="my-3" variant="success" type="submit">
		                    Login
		                </Button>
		                : 
		                <Button id='loginbtnCss submitBtn' className="my-3" variant="danger" type="submit" disabled>
		                    Login
		                </Button>
	            }
	    	</Form>		
		</div>	
		</>
    )
}
