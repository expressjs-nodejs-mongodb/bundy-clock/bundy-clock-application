import './App.css';

import Navbar from "./components/Navbar";
import TimeIn from './components/TimeIn'
import TimeOut from './components/TimeOut'

import Register from './Pages/Register';
import LogIn from './Pages/LogIn.js';
import Logout from './Pages/LogOut';
import TimeLogs from './Pages/TimeLogs.js';
import Admin from './Pages/Admin';
import AdminTL from './Pages/AdminTL';

import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import {UserProvider} from './UserContext';
import {useState, useEffect} from 'react';



function App() {

  const [user, setUser] = useState({

    // token:localStorage.getItem('token') 
    id: null,
    isAdmin: null
  })
  
  const unsetUser = () => {
      localStorage.clear();
  }
  
  // Used to check if the user information is properly stored upon login and the localStorage information is cleared upon logout
  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  })
  
  //  To update the User state upon a page load is initiated and a user already exists.
  useEffect(()=>{
  
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers:{
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);
  
      // Set the user states value if the token already exists in the local storage
      if(data._id !== undefined){
        setUser({
          id: data._id,
  isAdmin: data.isAdmin
        });
      }
      // set back to the inital state of the user if no token found in the local storage.
      else{
        setUser({
          id: null,
          isAdmin: null
        });
      }
      
    })
  
  }, [])

  return (
    <div className="App">
     <UserProvider value={{user, setUser, unsetUser}}>
     <>
    <Router>
      <Navbar/>
    <Routes>
    <Route exact path="/" element={<LogIn/>}/>

    <Route exact path="/register" element={<Register/>}/>
    <Route exact path="/login" element={<LogIn/>}/>
    <Route exact path="/timelogs" element={<TimeLogs/>}/>
    <Route exact path="/logout" element={<Logout/>}/>
    <Route exact path="/admin" element={<Admin/>}/>
    <Route exact path="/admintl" element={<AdminTL/>}/>

    <Route exact path="/timein" element={<TimeIn/>}/>
    <Route exact path="/timeout" element={<TimeOut/>}/>
    </Routes>
      {/* <Footer/> */}
    </Router>
    </>
    </UserProvider>
    </div>
  );
}

export default App;
